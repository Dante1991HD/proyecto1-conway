#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import random
import time
import os


'''Función que copia la matriz que contiene los datos de las celulas a ser
escrutinadas por la función "vecinas"'''


def copia_matriz(matriz_celulas):
    copia_matriz_celulas = []
    for i in range(len(matriz_celulas)):
        copia_matriz_celulas.append([])
        for j in range(len(matriz_celulas)):
            copia_matriz_celulas[i].append(matriz_celulas[i][j])
    return copia_matriz_celulas


# función que imprime la matriz
def impresora(matriz_celulas):
    time.sleep(.7)
    os.system('clear')
    print("Células vivas:", contador(matriz_celulas))
    print("muertas:", len(matriz_celulas) ** 2 - contador(matriz_celulas))
    for i in range(len(matriz_celulas)):
        for j in range(len(matriz_celulas[i])):
            print(matriz_celulas[i][j], end='  ')
        print()
    print()


# Función que cuenta la cantidad de células vivas
def contador(matriz_celulas):
    cantidad_celulas_vivas = 0
    for i in range(int(len(matriz_celulas))):
        cantidad_celulas_vivas += matriz_celulas[i].count('x')
    return cantidad_celulas_vivas


# Función que verifica la cantidad de vecinos vivos alrededor de una celula
def vecinas(matriz_celulas, posicion_x, posicion_y):
    vivas = 0
    for x in range(posicion_x-1, posicion_x + 2, 1):
        for y in range(posicion_y-1, posicion_y + 2, 1):
            if (x >= len(matriz_celulas) or y >= len(matriz_celulas) or
               y < 0 or x < 0 or (posicion_y == y and posicion_x == x)):
                continue
            elif matriz_celulas[x][y] == 'x':
                vivas += 1
    return vivas


# Funcion que aplica las reglas del juego
def celulas(matriz_celulas):
    matriz_celulas_copiada = copia_matriz(matriz_celulas)
    for i in range(len(matriz_celulas)):
        for j in range(len(matriz_celulas)):
            vivas = vecinas(matriz_celulas_copiada, i, j)
            if matriz_celulas[i][j] == 'x':
                if 1 < vivas < 4:
                    matriz_celulas[i][j] = 'x'
                else:
                    matriz_celulas[i][j] = '.'
            elif vivas == 3:
                matriz_celulas[i][j] = 'x'
    impresora(matriz_celulas)
    if matriz_celulas_copiada == matriz_celulas:
        print("config estable")
        time.sleep(2.4)
        crearmatriz()
    elif contador(matriz_celulas) == 0:
        print("Todas han muerto u.u 'Game Over'")
        time.sleep(2.4)
        crearmatriz()
    else:
        celulas(matriz_celulas)

'''Función que genera una lista sobre lista y la rellena
con 'x' = células vivas y '.' =  células muertas, luego lo imprime y
lo envía a la funcíón celulas'''


def crearmatriz():
    matriz_celulas = []
    N = random.randint(5, 15)
    for i in range(N):
        matriz_celulas.append([])
        for j in range(N):
            matriz_celulas[i].append(random.choice(['x', '.']))
    impresora(matriz_celulas)
    celulas(matriz_celulas)

crearmatriz()

